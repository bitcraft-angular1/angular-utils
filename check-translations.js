#!/usr/bin/env node

'use strict';

var fs = require('fs');

/**
 *
 * @param {{}} totalKeys
 * @param {ContentHolder} contentHolder
 */
function addToKeys(totalKeys, contentHolder) {
    var key;
    var value;

    for (key in contentHolder.content) {
        if (contentHolder.content.hasOwnProperty(key)) {
            value = contentHolder.content[key];

            if (typeof value === 'object') {
                if (!totalKeys[key]) {
                    totalKeys[key] = {};
                }

                addToKeys(totalKeys[key], {content: value, filename: contentHolder.filename});
            } else {
                if (!totalKeys[key]) {
                    totalKeys[key] = [];
                }
                totalKeys[key].push({translation: value, filename: contentHolder.filename});
            }
        }
    }
}

/**
 *
 * @param {Array<AnnotatedTranslation>} value
 * @param {ContentHolder} contentHolder
 * @returns {string}
 */
function valueStrictCheck(value, contentHolder) {
    var i;
    var currentValue;
    var res = '';

    for (i = 0; i < value.length; i++) {
        if (value[i].filename === contentHolder.filename) {
            currentValue = value[i].translation;
            break;
        }
    }

    for (i = 0; i < value.length; i++) {
        if (value[i].filename !== contentHolder.filename && value[i].translation !== '' && value[i].translation === currentValue) {
            res = ' has a duplicate (\'' + value[i].translation + '\') in ' + value[i].filename + '\n';
            value[i].translation = ''; // avoid duplicates report
            break;
        }
    }

    return res;
}

/**
 * @param {string} key
 * @param {Array<string>} deepKeys
 * @param {{}} translateIgnore
 * @returns {boolean}
 */
function isIgnored(key, deepKeys, translateIgnore) {
    var i;

    for (i = 0; i < deepKeys.length; i += 1) {
        if (!translateIgnore[deepKeys[i]]) {
            return false;
        }
        translateIgnore = translateIgnore[deepKeys[i]];
    }

    return translateIgnore[key] === true;
}

/**
 * @param {{}} totalKeys
 * @param {ContentHolder} contentHolder
 * @param {Array<string>} deepKeys
 * @param {boolean} isStrict
 * @param {{}} translateIgnore
 * @returns {string}
 */
function compareKeys(totalKeys, contentHolder, deepKeys, isStrict, translateIgnore) {
    var key;
    var value;
    var result = '';
    var valueStrictCheckRes;

    for (key in totalKeys) {
        if (totalKeys.hasOwnProperty(key) && !isIgnored(key, deepKeys, translateIgnore)) {
            value = totalKeys[key];

            if ((value instanceof Array) === false) { // deepObject
                deepKeys.push(key);
                result += compareKeys(totalKeys[key], {content: contentHolder.content[key], filename: contentHolder.filename}, deepKeys, isStrict, translateIgnore);
                deepKeys.pop();
            } else {
                if (!contentHolder.content[key]) {
                    if (deepKeys.length) {
                        result += deepKeys.join('.') + '.' + key + ' is missing\n';
                    } else {
                        result += key + ' is missing\n';
                    }
                } else {
                    if (isStrict) {
                        valueStrictCheckRes = valueStrictCheck(value, contentHolder);
                        if (valueStrictCheckRes !== '') {
                            if (deepKeys.length) {
                                result += deepKeys.join('.') + '.' + key + valueStrictCheckRes;
                            } else {
                                result += key + valueStrictCheckRes;
                            }
                        }
                    }
                }
            }
        }
    }

    return result;
}

/** @type {Array<ContentHolder|string>} */
var args = process.argv.slice(2);
var i;
var totalKeys = {};
var result;

var strictMode = false;
var translateIgnore = null;

for (i = 0; i < args.length; i += 1) {
    if (args[i].substring(0, 2) === '--') {
        if (args[i] === '--strict-mode') {
            strictMode = true;
        } else if (args[i] === '--i') {
            if (i + 1 < args.length) {
                translateIgnore = args.splice(i + 1, 1);
            } else {
                console.log('Skip invalid argument usage ' + args[i]);
            }
        } else {
            console.log('Skip unknown argument ' + args[i]);
        }

        args.splice(i, 1);
        i -= 1;
    }
}

if (translateIgnore !== null) {
    translateIgnore = require(process.cwd() + '/' + translateIgnore);
} else {
    translateIgnore = {};
}

for (i = 0; i < args.length; i += 1) {
    args[i] = { content: require(process.cwd() + '/' + args[i]), filename: args[i]};
    addToKeys(totalKeys, args[i]);
}

for (i = 0; i < args.length; i += 1) {
    console.log('Translations errors for ' + args[i].filename);
    result = compareKeys(totalKeys, args[i], [], strictMode, translateIgnore);

    if (result === '') {
        console.log('None');
    } else {
        console.log(result);
    }
}

/**
 * @typedef {Object} ContentHolder
 * @property {string} filename
 * @property {Object} content
 */

/**
 * @typedef {Object} AnnotatedTranslation
 * @property {string} filename
 * @property {string} translation
 */
