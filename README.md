# This component #

This component is only destined to be used as an utility for the other angular-bitcraft components. In other word, this module is only supposed to be a devDependency.

This modules provides :

* `build.sh`, a script that will build a browser version of a component. It works using browserify.

* `check-translations.js`, a small utility that tracks missing translations.

* `README.md`, this file.

# `check-translations.js` #

Usage: node check-translations.js path-to-translations-files [ --strict-mode [ --i path-to-ignore-file ]]

Using it with the flag `--strict-mode` will also compare the values of the translations. If they match it'll report an error.
Using it with the flag `--i` allow to specify a ignore file. It will skip the errors encountered on these keys.
This is useful for keys like `OK`.

# Make a component #

## Client side ##

Here are the basic guidelines to publish the client side of a component :

1. Create a `bower.json` file. It will be used to fetch your component with bower.
2. Add the latest version of `angular-utils` (this repository) as a devDependency
of your project.
3. Put your Angular files inside an `src` folder.
4. Commit your files.
5. Build your project, using the `build.sh` script delivered with this project.
6. Amend your commit (or recommit) to add the file you've just built.
7. Tag your last commit with your version number. (`git tag -a x.y.z -m "message"`)
8. Push your project.
9. Push your tag. (`git push origin x.y.z`)

## Server side (with laposte) ##

Here are the basic guidelines to publish the servers side of a component :

1. Create a `package.json` file. It'll be used to fetch your component with npm.
2. Put your plugins wherever. Inside a `plugins` folder?
3. Commit your files.
3. Tag your last commit with your version number. (`git tag -a x.y.z -m "message"`)
4. Push your project.
5. Push your tag. (`git push origin x.y.z`)

# Use a component in another project #

## Client side ##

Here are the basic guidelines to use a component.

1. Add the component as a dependency in your `bower.json`, referencing the tag you want to use. (`"component-name": "ssh://path-to-component.git#x.y.z"`)
2. Add the component as a dependency of your application (angular-related).
3. Add the component in a script balise in your index.html (`<script type="text/javascript" src="path-to-component.js"></script>`)

## Server side ##

Here are the basic guidelines to use a component.

1. Add the component as a dependency in your `package.json`, referencing the tag you want to use. (`"component-name": "ssh://path-to-component.git#x.y.z"`)
2. Change your exports to an object containing the plugins, and a setup function.
3. Make a proxy plugin, this will reexport the plugins of the component in the
context of the other application and call your setup. Hint code:

```javascript
// In your component
module.exports = {
    plugins: {
        x: {
            method: 'POST',
            callback: x,
            anonymous: true,
            contentType: 'application/json'
        }
    },
    setup: setup
};


// In your app
var component = require('component-name/plugins/monitoring.js');

component.setup(args);

for (key in component.plugins) {
    if (component.plugins.hasOwnProperty(key)) {
        module.exports[key] = component.plugins[key];
    }
}
```
