#!/usr/bin/env bash
if [ "$#" != "1" ]; then
    echo "Usage: build.sh output-name"
    exit 2
fi

git diff-index --quiet HEAD --
if [ "$?" != "0" ]; then
    echo "Please commit your changes before running this script. ABORTED"
    exit 1
fi

#set -x
#trap read debug

shopt -s globstar

OUTPUT_PATH="$1"

for i in src/**/*.directive.js src/**/*.component.js; do
    CURRENT_COMPONENT=$i
    CURRENT_TEMPLATE=`echo $i | sed 's,\(component.js\|directive.js\),template.html,'`
    if [ -f "$CURRENT_TEMPLATE" ]; then
        mv "$CURRENT_COMPONENT" "$CURRENT_COMPONENT.bak"
        LINE_NBR_TOT=`wc -l < "$CURRENT_COMPONENT.bak"`
        LINE_NBR_HEAD=`awk '/use strict/ {print FNR}' "$CURRENT_COMPONENT.bak"`
        ((LINE_NBR_TAIL="$LINE_NBR_TOT"-"$LINE_NBR_HEAD"))
        head -n "$LINE_NBR_HEAD" "$CURRENT_COMPONENT.bak" > "$CURRENT_COMPONENT"
        echo "" >> "$CURRENT_COMPONENT"
        echo "var template = '' +" >> "$CURRENT_COMPONENT"
        cat "$CURRENT_TEMPLATE" | sed 's/\r$//' | sed '/^$/d' | sed 's|\x27|\\\x27|g' | sed "s|.*|'&' +|" >> "$CURRENT_COMPONENT"
        echo "'';" >> "$CURRENT_COMPONENT"
        echo "" >> "$CURRENT_COMPONENT"
        tail -n "$LINE_NBR_TAIL" "$CURRENT_COMPONENT.bak" >> "$CURRENT_COMPONENT"
        sed -i 's|        templateUrl.*|        template: template,|' "$CURRENT_COMPONENT"
    fi
done

browserify src/index.js -o "$OUTPUT_PATH"

for i in src/**/*.directive.js src/**/*.component.js; do
    CURRENT_COMPONENT=$i
    if [ -f "$CURRENT_COMPONENT.bak" ]; then
        mv "$CURRENT_COMPONENT.bak" "$CURRENT_COMPONENT"
    fi
done
